# Grocerydown
[![pipeline status](https://gitlab.com/k-cybulski/grocerydown/badges/master/pipeline.svg)](https://gitlab.com/k-cybulski/grocerydown/-/commits/master)
[![coverage report](https://gitlab.com/k-cybulski/grocerydown/badges/master/coverage.svg)](https://gitlab.com/k-cybulski/grocerydown/-/commits/master)

A tool to compile markdown-formatted meal plan into a shopping list. A meal plan is
understood to be a list of meals to prepare along with a list of necessary
ingredients. The resultant shopping list aggregates all ingredients necessary and
puts them into a lovely markdown file, with products from various categories you may
encounter close in a store together listed together.

Designed to make groceries for longer periods of time easier, especially if used in
conjunction with [CodiMD](https://github.com/hackmdio/codimd) for checkboxes which
can be easily checked out while in the store.
