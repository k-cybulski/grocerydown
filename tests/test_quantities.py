import pytest

from grocerydown.quantities import (Quantity, Measure, parse_quantity, 
                                    QuantitySet, parse_quantity_set)

def test_quantity_eq():
    assert (Quantity([Measure(1, 'l')], 'cat') 
            == Quantity([Measure(1, 'l')], 'cat'))
    assert (Quantity([Measure(1, 'l'), Measure(2, 'g')], 'cat') 
            != Quantity([Measure(1, 'l')], 'cat'))
    assert (Quantity([Measure(1, 'l'), Measure(2, 'g')], 'cat') 
            == Quantity([Measure(1, 'l'), Measure(2, 'g')], 'cat'))
    assert (Quantity([Measure(1, 'l')], 'cat') 
            != Quantity([Measure(1, 'l')], 'dog'))

def test_parse_quantity():
    cases = [
        ('1 litre of water', Quantity([Measure(1, 'l')], 'water')),
        ('1 Litre of water', Quantity([Measure(1, 'l')], 'water')),
        ('1 l cat', Quantity([Measure(1, 'l')], 'cat')),
        ('5 grams of dog', Quantity([Measure(5, 'g')], 'dog'))
    ]
    for str_, target in cases:
        assert parse_quantity(str_) == target

def test_quantity_add():
    cases = [
        ([parse_quantity('1 litre of water'), 
          parse_quantity('1.5 l water')], parse_quantity('2.5 l water')),
        ([parse_quantity('5 pieces of wood'), 
          parse_quantity('3 cups of wood'),
          parse_quantity('6 teaspoons of wood')], Quantity([
              Measure(5, 'piece'), Measure(3, 'cup'), Measure(6, 'tsp')],
              'wood'))
    ]
    for quants, target in cases:
        sum_ = quants[0]
        for q in quants[1:]:
            sum_ += q
        assert sum_ == target

def test_quantity_mul():
    cases = [
        (parse_quantity('1 l of water'), 5, parse_quantity('5 l of water'))
    ]
    for src, times, target in cases:
        assert src * times == target

def test_quantity_set_eq():
    coll = [Quantity([Measure(1, 'l')], 'cat'),
            Quantity([Measure(1, 'l')], 'dog')]
    qs = QuantitySet(coll)
    assert qs == QuantitySet(coll)
    assert qs != QuantitySet(coll + [Quantity([Measure(1, 'l')], 'elephant')])

@pytest.fixture
def sample_q_strs():
    ls = [
"""1 litre milk
10g curcuma""",
"""300g buckwheat
1 onion
3 cloves garlic
1 canned chopped tomatoes
1 fresh tomato
1 carrot
1/2 jar of black olives
handful of peas
red kidney beans
oil""",
"""300g basmati rice
1 can of lima beans
1 white onion
3 cloves of garlic
turmeric (curcuma)
0.5 leeks
powder paprika
1 carrot
handful of peas
1 pastinak
1/2 bell pepper"""]
    return list(map(lambda x: x.splitlines(), ls))

@pytest.fixture
def sample_qs(sample_q_strs):
    return [parse_quantity_set(i) for i in sample_q_strs]

def test_parse_quantity_set_eq(sample_q_strs, sample_qs):
    for str_, qs in zip(sample_q_strs, sample_qs):
        assert parse_quantity_set(str_) == qs

def test_quantity_set_add():
    to_add = [
"""1 litre milk
10g curcuma""",
"""5 g curcuma
2 bags of black tea""",
"""0.5 litre milk"""]
    expected_sum = parse_quantity_set("""1.5 litre milk
15g curcuma
2 bags of black tea""".splitlines())
    qs = [parse_quantity_set(str_.splitlines()) for str_ in to_add]
    sum_ = qs[0]
    for q in qs[1:]:
        sum_ += q
    assert sum_ == expected_sum

def test_quantity_set_mul():
    cases = [
        (
"""1 litre milk
10g curcuma""", 5, 
"""5 litres of milk
50g curcuma""")
    ]
    for in_str, times, out_str in cases:
        in_qs = parse_quantity_set(in_str.splitlines()) 
        out_qs = parse_quantity_set(out_str.splitlines())
        assert in_qs * times == out_qs
