import pytest

from grocerydown.markdown import sections_from_lines
from grocerydown.quantities import parse_quantity_set
from grocerydown.meals import aggregate_quantities_from_sections

@pytest.fixture
def sample_sections():
    return sections_from_lines("""# Meals 12.08.2030

- [ ] 2 Veggies with buckwheat
- [ ] 
- [x] 2 Gnocchi with bell pepper and spring onion
- [x]
- [ ] 2 Simple Veggies with couscous
- [ ]

## Veggies with buckwheat
 - 300g buckwheat
 - 1 onion

## Gnocchi with bell pepper and spring onion
- 1 packages of Gnocchi
- 2 spring onions

## Simple Veggies with couscous
 - 300g couscous""".splitlines())

def test_aggregate_quantities(sample_sections):
    quants = aggregate_quantities_from_sections(sample_sections)
    correct_quants = parse_quantity_set([
        "600 g of buckwheat", "2 white onions", "2 packages of Gnocchi",
        "4 spring onions", "600g of couscous"])
    assert quants == correct_quants
