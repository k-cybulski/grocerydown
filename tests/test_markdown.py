import pytest

from grocerydown.markdown import remove_links, sections_from_lines, Section

def test_remove_links():
    cases = [
        ("cats [2 Apple Crumble](https://utopia.de/ratgeber/apple-crumble-ein-rezept-mit-veganer-variante/) dogs", "cats 2 Apple Crumble dogs")]
    for in_, out in cases:
        assert remove_links(in_) == out

@pytest.fixture
def sample_lines():
    lines = """# Meals 12.08.2030

- [ ] 2 Veggies with buckwheat
- [ ] 
- [x] 2 Gnocchi with bell pepper and spring onion
- [x]
- [ ] 2 Simple Veggies with couscous
- [ ]

## Veggies with buckwheat
 - 300g buckwheat
 - 1 onion

## Gnocchi with bell pepper and spring onion
- 1 packages of Gnocchi

## Simple Veggies with couscous
 - 300g couscous""".splitlines()
    return lines

def test_parse_sections(sample_lines):
    assert sections_from_lines(sample_lines) == [
        Section(name='Meals 12.08.2030',
                content=['2 Veggies with buckwheat', '2 Gnocchi with bell pepper and spring onion', '2 Simple Veggies with couscous']),
        Section(name='Veggies with buckwheat', content=['300g buckwheat', '1 onion']),
        Section(name='Gnocchi with bell pepper and spring onion',
                content=['1 packages of Gnocchi']),
        Section(name='Simple Veggies with couscous', content=['300g couscous'])]

