"""
meals.py
========
Module for dealing with meal plans.

A meal plan is a markdown document composed of two parts:
    - List of meal names with the number of times they will be prepared under
      the first header
    - Headers with lists of ingredients per meal, mentioned in the first
      section
"""
import re

from .util import parse_numbers
from .quantities import parse_quantity_set, aggregate_quantity_sets

def _parse_meal_numbers(meal_list):
    NUMBER_AND_MAIN_PART_SPLIT = r'^([0-9\W\./]*)(.+)'
    meal_numbers = {}
    for number_str, meal in map(
        lambda str_: re.match(NUMBER_AND_MAIN_PART_SPLIT, str_).groups(),
                            meal_list):
        number = parse_numbers(number_str)
        meal_numbers[meal.lower()] = number
    return meal_numbers

def meal_numbers_from_sections(sections):
    return _parse_meal_numbers(sections[0].content)

def recipes_from_sections(sections):
    return {
        section.name.lower(): parse_quantity_set(section.content)
        for section in sections[1:]
    }

def aggregate_quantities_from_sections(sections):
    meal_numbers = meal_numbers_from_sections(sections)
    recipes = recipes_from_sections(sections)
    return aggregate_quantity_sets([
        recipes[meal] * meal_numbers[meal] for meal in meal_numbers
    ])
