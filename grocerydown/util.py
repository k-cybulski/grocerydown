import re

def parse_numbers(string):
    # Now, find all number-looking things
    number_strings = re.findall(r'[0-9/.]+', string)
    if len(number_strings) > 1:
        return sum([parse_numbers(str_) for str_ in number_strings])
    elif len(number_strings) == 1:
        str_ = number_strings[0]
        if '/' in str_:
            numerator, denominator = str_.split('/')
            return float(numerator)/float(denominator)
        else:
            return float(str_)
    else:
        return 1.

def parse_synonyms_file(path):
    with open(path) as file_:
        lines = file_.readlines()
    unit_map = {}
    for line in lines:
        split = line.strip().split(',')
        for synonym in split:
            unit_map[synonym] = split[0]
            # Hardcoded dealing with plurals
            if synonym.endswith('s'):
                unit_map[synonym + 'es'] = split[0]
            else:
                unit_map[synonym + 's'] = split[0]
    return unit_map

