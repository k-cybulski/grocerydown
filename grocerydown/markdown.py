"""
parser.py
=========
Module for parsing markdown files.
"""
import re
from enum import Enum, auto
from collections import namedtuple

def remove_links(str_):
    pattern = r"\[([^\[\]]*?)\]\(.+\)"
    return re.sub(pattern, lambda match: match.group(1), str_)

class Linetype(Enum):
    HEADER = r"^ *#+ +(.+)"
    ENTRY = r"^ *[- ]+(?:\[[ x]\] *| *(?!\[[ x]\]))(\S.+)"

    def match(self, str_):
        match = re.match(self.value, str_)
        if match:
            return remove_links(match.group(1)).strip()
        else:
            return None

def _linetype(line):
    for type_ in Linetype:
        if type_.match(line):
            return type_

Section = namedtuple('Section', ['name', 'content'])

def sections_from_lines(lines):
    sections = []
    name = None
    contents = []
    for line in lines:
        type_ = _linetype(line)
        if type_ == Linetype.HEADER:
            if name is not None:
                sections.append(Section(name, contents))
            name = type_.match(line)
            contents = []
        elif type_ == Linetype.ENTRY:
            contents.append(type_.match(line))
    if name is not None:
        sections.append(Section(name, contents))
    return sections

def sections_from_file(file_):
    return sections_from_lines(file_.readlines())

def sections_from_path(path):
    with open(path) as file_:
        return sections_from_file(file_)
