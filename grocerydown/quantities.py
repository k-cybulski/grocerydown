"""
quantities.py
=============
Code for parsing quantities of things from strings.
"""
from collections import namedtuple
import re
from copy import copy
from pathlib import Path
from fuzzywuzzy import process

from .util import parse_numbers, parse_synonyms_file

# Operations on units of measure

def get_default_unit_map():
    path = Path(__file__).with_name('units')
    return parse_synonyms_file(path)

def _parse_unit_map_regex(unit_map):
    # Enforce order of regex checks
    keys = sorted(unit_map.keys(), key=lambda x: len(x), reverse=True)
    return re.compile('|'.join([r' ?{} (?:of |)'.format(key) for key in keys]),
                     flags=re.I)

def _get_sub_func(unit_map):
    def sub(match):
        str_ = match.group(0)
        if str_.endswith(' of '):
            str_ = str_[:-4]
        return r'{} '.format(unit_map[str_.strip().lower()])
    return sub

def simplify_units(string, unit_map):
    regex = _parse_unit_map_regex(unit_map)
    sub = _get_sub_func(unit_map)
    out_str = regex.sub(sub, string)
    return out_str

# Operations on quantities of things

Measure = namedtuple('Measure', ['value', 'unit'])

class Quantity:
    """An amount of something."""
    def __init__(self, measures, item):
        self._measure_map = {}
        for measure in measures:
            self._measure_map[measure.unit] = measure
        self._item = item

    def __add__(self, other_quantity):
        if other_quantity._item != self._item:
            raise ValueError("Cannot add quantities of different items: {} and {}".format(
                self._item, other_quantity._item))
        out_measure_map = copy(self._measure_map)
        for measure in other_quantity._measure_map.values():
            if measure.unit not in self._measure_map:
                out_measure_map[measure.unit] = measure
            else:
                out_measure_map[measure.unit] = Measure(
                    self._measure_map[measure.unit].value + measure.value,
                    measure.unit)
        return Quantity(out_measure_map.values(), self._item)

    def __mul__(self, times):
        out_measure_map = {}
        for unit, measure in self._measure_map.items():
            out_measure_map[unit] = Measure(
                measure.value * times, unit)
        out_quantity = Quantity(out_measure_map.values(), self._item)
        return out_quantity

    def __str__(self):
        measure_strs = []
        for measure in self._measure_map.values():
            if measure.unit is not None:
                measure_strs.append("{} {}".format(measure.value, measure.unit))
            else:
                measure_strs.append("{} units".format(measure.value))
        return " and ".join(measure_strs) + " of " + self._item

    def __repr__(self):
        return "<Quantity:{}>".format(self.__str__())

    def __eq__(self, other):
        if type(self) == type(other):
            if self._item != other._item:
                return False
            if set(self._measure_map.keys()) == set(other._measure_map.keys()):
                for key in self._measure_map.keys():
                    if self._measure_map[key] != other._measure_map[key]:
                        return False
                return True
        return False

def _extract_unit_substring(string):
    """Extracts the part of the string which may include units.
    That is, all numbers on the left and the first word on the left.
    """
    match = re.search(r'(^[0-9/. ]* *[^^\d\s/.]+)', string, flags=re.I)
    if match:
        return match.group(1)

def _parse_value(string):
    unit_substring = _extract_unit_substring(string)
    return parse_numbers(unit_substring)

def _parse_unit(string, unit_map):
    unit_substring = _extract_unit_substring(string)
    candidate_match = re.search(r'([^\d\s/.]+)', string)
    if candidate_match:
        group = candidate_match.group(1).lower()
        if group in unit_map:
            return unit_map[group]

def _parse_item_raw(string, unit_map, unit):
    simplified_string = simplify_units(string, unit_map)
    if unit:
        item_string = simplified_string[simplified_string.index(unit) +
                                        len(unit) + 1:]
    else:
        match = re.search(r'^[0-9/. ]* *(.+)',
                                simplified_string)
        if match:
            item_string = match.group(1)
        else:
            return None
    item_string = item_string.strip()
    return item_string

# Dealing with synonyms of foodstuffs
def get_default_item_map():
    path = Path(__file__).with_name('synonyms')
    return parse_synonyms_file(path)

def get_synonym(item, item_map):
    suggestion, score = process.extractOne(item, item_map.keys())
    if score < 95:
        return item
    return item_map[suggestion]

def _parse_item(string, unit_map, unit, item_map):
    item = _parse_item_raw(string, unit_map, unit)
    return get_synonym(item, item_map)

def parse_quantity(string, unit_map=None, item_map=None):
    if unit_map is None:
        unit_map = get_default_unit_map()
    if item_map is None:
        item_map = get_default_item_map()
    value = _parse_value(string)
    unit = _parse_unit(string, unit_map)
    measure = Measure(value, unit)
    item = _parse_item(string, unit_map, unit, item_map)
    return Quantity([measure], item)

# Operations on sets of quantities of things, e.g. ingredients for a recipe

class QuantitySet:
    def __init__(self, collection=None):
        self._quantities = {}
        if collection:
            self.add_collection(collection)
        
    def add_quantity(self, quantity):
        if quantity._item not in self._quantities:
            self._quantities[quantity._item] = quantity
        else:
            self._quantities[quantity._item] += quantity

    def add_collection(self, quantities):
        for quantity in quantities:
            self.add_quantity(quantity)

    def __add__(self, other_quantity_set):
        qs = QuantitySet(self._quantities.values())
        qs.add_collection(other_quantity_set._quantities.values())
        return qs

    def __mul__(self, times):
        out_quantities = {
            item: quantity * times
            for item, quantity in self._quantities.items()}
        return QuantitySet(out_quantities.values())

    def __str__(self):
        return ", ".join([quant.__str__() for quant in
                          self._quantities.values()])

    def __repr__(self):
        return "<QuantitySet:{}>".format(self.__str__())

    def __eq__(self, other):
        if type(self) == type(other):
            # each quantity already keeps track of equality well, so this
            # should work
            return self._quantities == other._quantities
        else:
            return False

def parse_quantity_set(coll_of_string, unit_map=None, item_map=None):
    return QuantitySet([parse_quantity(str_, unit_map, item_map) for str_ in
                        coll_of_string])

def aggregate_quantity_sets(coll_of_qs):
    sum_ = None
    for qs in coll_of_qs:
        if sum_ is None:
            sum_ = qs
        else:
            sum_ += qs
    return sum_
